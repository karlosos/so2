// SO2 IS1 212A LAB08
// Karol Dzialowski
// dk39259@zut.edu.pl

#include <sys/socket.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void* writeListener(void *arguments);
void* readListener(void *arguments);

int main(int argc , char *argv[])
{
    char* address_ip;
    char* name_in;
    int port;
    int ret, index;

    opterr = 0;

    while ((ret = getopt(argc, argv, "a:p:n:")) != -1) {
        switch (ret) {
            case 'a':
                address_ip = optarg;
                break;
            case 'p':
                port = atoi(optarg);
                break;
            case 'n':
                name_in = optarg;
                break;
            case '?':
                if (optopt == 'a')
                    fprintf(stderr, "Opcja -%c wymaga argumentu.\n", optopt);
                else if (optopt == 'p')
                    fprintf(stderr, "Opcja -%c wymaga argumentu.\n", optopt);
                else if (optopt == 'n')
                    fprintf(stderr, "Opcja -%c wymaga argumentu.\n", optopt);
                else
                    fprintf(stderr, "Uknown option '-%c'.\n", optopt);
                return 1;
            default:
                abort();
        }
    }

    printf("%s, %s, %d \n", name_in, address_ip, port);
    char *name = malloc(sizeof(char) * (strlen(name_in) + 10));
    // ustawia komende
    sprintf(name, "/setname %s", name_in);

    int socket_main;
    struct sockaddr_in server_addr;

    // Stworzenia gwiazda
    socket_main = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_main == -1)
    {
        printf("Nie mozna stworzyc gniazda \n");
    }
    printf("Gniazdo stworzone \n");

    // Ustawiamy adres i port
    server_addr.sin_addr.s_addr = inet_addr(address_ip);
    server_addr.sin_port = htons(port);
    server_addr.sin_family = AF_INET;

    // Polacz sie z serwerem
    if (connect(socket_main , (struct sockaddr *)&server_addr , sizeof(server_addr)) < 0)
    {
        perror("Nie mozna polaczyc sie z serwerem.");
        return 1;
    }

    // Wyslij do serwera nazwe uzytkownika
    if( send(socket_main , name , strlen(name) , 0) < 0)
    {
        printf("Nie mozna ustawic nazwy uzytkownika.");
        return 1;
    }

    printf("Polaczono z serwerem.\n");

    // Watek wysylania wiadomosci
    pthread_t write_listener;
    pthread_create(&write_listener, NULL, writeListener, (void *) &socket_main);

    // Watek odbierania wiadomosci
    pthread_t read_listener;
    pthread_create(&read_listener, NULL, readListener, (void *) &socket_main);

    pthread_join(write_listener, NULL);
    pthread_cancel(read_listener);

    close(socket_main);
    return 0;
}

void* writeListener(void *arguments) {
    int sock = * (int*) arguments;
    char message[1000];
    while(1)
    {
        fgets(message, 1000, stdin);

        // Usuwa znak nowej linii. Dodaje 0
        if ((strlen(message) > 0) && (message[strlen (message) - 1] == '\n'))
            message[strlen (message) - 1] = '\0';

        // Wyslij wiadomosc
        if( send(sock , message , strlen(message) , 0) < 0)
        {
            printf("Nie udalo sie wyslac wiadomosci");
            return NULL;
        }
    }
}

void* readListener(void *arguments) {
    int sock = * (int*) arguments;
    char server_reply[2000];
    while (1) {
        // Odbierz wiadomosc z serwera
        if (read(sock, server_reply, 2000) < 0) {
            printf("Nie udalo sie odebrac wiadomosci.");
            break;
        }

        printf("%s \n", server_reply);
        // Wyzeruj server_reply
        for (int i = 0; i < 2000; i++)
            server_reply[i] = '\0';
    }
}
