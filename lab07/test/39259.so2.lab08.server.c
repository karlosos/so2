// SO2 IS1 212A LAB08
// Karol Dzialowski
// dk39259@zut.edu.pl

// https://www.binarytides.com/multiple-socket-connections-fdset-select-linux/
#include <stdio.h>
#include <sys/time.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>


char *concat(char *s1, const char *s2);

int main(int argc , char *argv[])
{
    int port;
    int quit_flag = 0;
    int ret, index;

    opterr = 0;

    // Wczytywanie parametrow
    while ((ret = getopt(argc, argv, "qp:")) != -1) {
        switch (ret) {
            case 'q':
                quit_flag = 1;
                break;
            case 'p':
                port = atoi(optarg);
                break;
            case '?':
                if (optopt == 'p')
                    fprintf(stderr, "Opcja -%c wymaga argumentu.\n", optopt);
                else
                    fprintf(stderr, "Uknown option '-%c'.\n", optopt);
                return 1;
            default:
                abort();
        }
    }

    if (quit_flag) {
        system("killall server");
        return 0;
    }
    daemon(0, 0);

    int optname = 1;    // do getsockopt
    int master_socket;  // glowne gniazdo
    int new_socket;     // gniazdo przychodzace
    int client_socket[30]; // gniazda klientow
    char* client_names[30]; // nazwy klientow
    int max_clients = 30;   // maksymalna ilosc klientow
    int active;           // aktywnosc na gniezdzie
    int read_end;
    int socket_desc;
    int max_socket_desc;
    struct sockaddr_in address;
    int addr_len;        // dlugosc struct sockaddr_in address; do accept
    char* error_message = "Nie mozna wyslac wiadomosci";

    char buffer[1025];
    char buffer_cpy[1025];

    // zbior deskryptorow gniazd
    fd_set readfds;

    // wiadomosc
    char *message = "Witaj na serwerze! \n";

    // Inicjalizuj wszystkie client_socket[] na 0 czyli niesprawdzone
    for (int i = 0; i < max_clients; i++) {
        client_socket[i] = 0;
    }

    // Stworz glowne gniazdo
    if( (master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0) {
        perror("Blad gniazda glownego");
        exit(EXIT_FAILURE);
    }

    // Ustaw gniazdo zeby obslugiwalo wiele polaczen
    if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&optname, sizeof(optname)) < 0 ) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // Ustawienia gniazda
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    // Bindowanie serwera
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("Blad bindowania");
        exit(EXIT_FAILURE);
    }
    printf("Serwer nasluchuje na porcie: %d \n", port);

    // Maksymalnie 3 polaczenia oczekujace do serwera
    if (listen(master_socket, 3) < 0) {
        perror("Blad listen");
        exit(EXIT_FAILURE);
    }

    addr_len = sizeof(address);

    while(1) {
        // Wyczysc fd_set
        FD_ZERO(&readfds);

        // Dodaj master_socket do fd_set
        FD_SET(master_socket, &readfds);
        max_socket_desc = master_socket;

        // Dodaj sockety do fd_set
        for ( int i = 0 ; i < max_clients ; i++) {
            // Deskryptor socketu
            socket_desc = client_socket[i];

            // Najwiejszy numer deskryptora, potrzebny do select.
            if(socket_desc > max_socket_desc)
                max_socket_desc = socket_desc;

            // Jezeli poprawny deskryptor socketu to dodaj do listy czytania
            if(socket_desc > 0)
                FD_SET( socket_desc , &readfds);
        }

        // Czekaj na aktywnosc na jednym z gniazd. Timeout NULL wiec czeka nieskonczonosc
        active = select( max_socket_desc + 1 , &readfds , NULL , NULL , NULL);

        if ( (errno!=EINTR) && (active < 0)) {
            printf("Blad select");
        }

        // Polaczenia przychodzace (na master_socket)
        if (FD_ISSET(master_socket, &readfds)) {
            if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t*)&addr_len))<0) {
                perror("Blad accept");
                exit(EXIT_FAILURE);
            }

            // Wyslij wiadomosc powitalna
            if( send(new_socket, message, strlen(message), 0) != strlen(message) ) {
                perror("send");
            }

            // Dodaj nowe gniazdo do tablicy
            for (int i = 0; i < max_clients; i++) {
                // Dodaj do pierwszego wolnego miejsca
                if( client_socket[i] == 0 ) {
                    client_socket[i] = new_socket;
                    break;
                }
            }
        }

        // Odbieraj wiadomosci od klientow
        for (int i = 0; i < max_clients; i++) {
            socket_desc = client_socket[i];

            if (FD_ISSET( socket_desc , &readfds)) {
                // Zamykanie polaczenia
                if ((read_end = read( socket_desc , buffer, 1024)) == 0) {
                    getpeername(socket_desc , (struct sockaddr*)&address , (socklen_t*)&addr_len);
                    free(client_names[i]);
                    close(socket_desc);
                    client_socket[i] = 0;
                }
                // Wiadomosci
                else {
                    // Ustaw 0 na koncu danych czytanych
                    buffer[read_end] = '\0';

                    // Nowy uzytkownik (ustawienie nazwy)
                    if (strncmp(buffer, "/setname", 8) == 0) {
                        client_names[i] = malloc(sizeof(char) * (strlen(buffer)));
                        strcpy(client_names[i], buffer + 9);
                    }
                    // Lista uzytkownikow
                    else if (strcmp(buffer, "/list") == 0) {
                        char *user_list = malloc(sizeof(char));
                        user_list[0] = '\0';
                        for (int j = 0; j < max_clients; j++) {
                            if(client_socket[j] != 0) {
                                printf("%s \n", client_names[j]);
                                concat(user_list, client_names[j]);
                                concat(user_list, " ");
                            }
                        }
                        strcpy(buffer, user_list);
                        send(socket_desc , buffer , strlen(buffer) , 0 );
                    }
                    // Zwykla wiadomosc
                    else {
                        char * receiver_name;
                        strcpy(buffer_cpy, buffer);
                        receiver_name = strtok(buffer_cpy, " ");

                        // Szukaj odbiorcy
                        int send_flag = 0;
                        for (int j = 0; j< max_clients; j++) {
                            if (client_socket[j] != 0) {
                                if (strcmp(client_names[j], receiver_name) == 0) {
                                    char * message = malloc(sizeof(char));
                                    concat(message, client_names[i]);
                                    concat(message, ":");
                                    concat(message, buffer+strlen(receiver_name));
                                    send(client_socket[j] , message , strlen(message) , 0 );
                                    free(message);
                                    send_flag = 1;
                                }
                            }
                        }
                        if (send_flag == 0) {
                            send(socket_desc , error_message , strlen(error_message) , 0 );
                        }
                        printf("%s \n", receiver_name);
                    }
                }
            }
        }
    }

    return 0;
}


char *concat(char *s1, const char *s2) {
    const size_t a = strlen(s1);
    const size_t b = strlen(s2);
    const size_t size_ab = a + b + 1;

    s1 = realloc(s1, size_ab);

    memcpy(s1 + a, s2, b + 1);

    return s1;
}
