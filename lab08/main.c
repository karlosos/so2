// wyswietlic zawartosc superbloku
// 
#include <stdlib.h>
#include <assert.h>
#include <zconf.h>
#include "so2ext2.h"
#include "stdio.h"
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

struct ext2_inode entry_wezel;

void print_permissions(unsigned int file_mode) {
    if (S_ISREG(file_mode))
        printf("-");
    else if (S_ISDIR(file_mode))
        printf("d");
    else if (S_ISCHR(file_mode))
        printf("c");
    else if (S_ISBLK(file_mode))
        printf("b");
    else if (S_ISFIFO(file_mode))
        printf("p");
    else if (S_ISSOCK(file_mode))
        printf("s");
    else if (S_ISLNK(file_mode))
        printf("l");

    // user
    if(file_mode & S_IRUSR)
        printf("r");
    else
        printf("-");

    if(file_mode & S_IWUSR)
        printf("w");
    else
        printf("-");

    if(file_mode & S_IXUSR)
        printf("x");
    else
        printf("-");


    // group
    if(file_mode & S_IRGRP)
        printf("r");
    else
        printf("-");

    if(file_mode & S_IWGRP)
        printf("w");
    else
        printf("-");

    if(file_mode & S_IXGRP)
        printf("x");
    else
        printf("-");


    // all
    if(file_mode & S_IROTH)
        printf("r");
    else
        printf("-");

    if(file_mode & S_IWOTH)
        printf("w");
    else
        printf("-");

    if(file_mode & S_IXOTH)
        printf("x");
    else
        printf("-");

    printf(" ");
}

struct visitedDir {
    unsigned int inode;
    short has_been_visited;
    struct visited_dir* next;
    char file_name[EXT2_NAME_LEN + 1];
    int file_type;
};

struct visitedDir *visited_dir_first = NULL;
struct visitedDir *visited_dir_last = NULL;

void print_dirs() {
    struct visitedDir *vd;
    vd = visited_dir_first;
    while(vd != NULL) {
        printf("%d : %s \n", vd->inode, vd->file_name);
        vd = vd->next;
    }
}
void add_dir(unsigned int inode, char file_name[], int file_type) {
    if (visited_dir_first == NULL) {
        visited_dir_first = malloc(sizeof(struct visitedDir));
        visited_dir_first->inode = inode;
        visited_dir_first->has_been_visited = 0;
        visited_dir_first->next = NULL;
        visited_dir_first->file_type = file_type;
        strcpy(visited_dir_first->file_name, file_name);
        visited_dir_last = visited_dir_first;
        return;
    }

    struct visitedDir *vd;
    vd = visited_dir_first;
    while(vd != NULL) {
        if (vd->inode == inode)
            return;
        vd = vd->next;
    }


    struct visitedDir *new_visited_dir = malloc(sizeof(struct visitedDir));
    new_visited_dir->inode = inode;
    new_visited_dir->has_been_visited = 0;
    strcpy(new_visited_dir->file_name, file_name);
    new_visited_dir->next = NULL;
    new_visited_dir->file_type = file_type;
    visited_dir_last->next = new_visited_dir;
    visited_dir_last = new_visited_dir;
}

int fd; // obraz dysku
struct ext2_super_block super;  // superblok
struct ext2_group_desc* group_descs;    // tablica z deskryptorami grup

unsigned int block_size;        // wielkosc bloku
unsigned int inodes_per_block;  // liczba i-wezlow na blok
unsigned int itable_blocks;     // rozmiar w blokach tablicy i-wezlow
struct ext2_inode root_inode;   // i-wezel do folderu root

/**
 * Oblicza offset do lseek dla podanego bloku
 * @param block
 * @return
 */
unsigned int blockOffset(int block) {
    //#define BLOCK_OFFSET(block) (BASE_OFFSET + (block-1)*block_size)
    return 1024 + (block-1)*block_size;
}

/**
 * Otwiera obraz dysku
 */
void openDisk() {
    fd = open("disk-image2", O_RDONLY);
}

/**
 * Czyta superblok
 */
void readSuperBlock() {
    lseek(fd, 1024, SEEK_SET);
    read(fd, &super, sizeof(super));
    block_size = 1024 << super.s_log_block_size;
    // liczba i-wezlow na blok
    inodes_per_block = block_size / sizeof(struct ext2_inode);

    // rozmiar w blokach tablicy i-wezlow
    itable_blocks = super.s_inodes_per_group / inodes_per_block;

}

/**
 * Czytaj deskryptory grip
 */
void readGroups() {
    // liczba grup blokow
    unsigned int group_count = 1 + (super.s_blocks_count-1) / super.s_blocks_per_group;

    // rozmiar listy deksryptorow (nie uzywam bo calloc)
    unsigned int descr_list_size = group_count * sizeof(struct ext2_group_desc);

    lseek(fd, 1024 + block_size, SEEK_SET);

    //int group_count = super.s_inodes_count/super.s_inodes_per_group;
    group_descs = calloc(group_count, sizeof(struct ext2_group_desc));

    for (int i=0; i<group_count; i++) {
        read(fd, &group_descs[i], sizeof(struct ext2_group_desc));
    }
}

/**
 * Czytaj inode z podanej grupy o podanym numerze
 * @param inode_no
 * @param group
 * @param inode
 */
void readInode(int inode_no, struct ext2_group_desc *group, struct ext2_inode *inode) {
    lseek(fd, blockOffset(group->bg_inode_table) + (inode_no-1)* sizeof(struct ext2_inode), SEEK_SET);
    read(fd, inode, sizeof(struct ext2_inode));
}

/**
 * Czytaj katalog root
 */
void readRoot() {
    readInode(2, &group_descs[0], &root_inode);
    if (S_ISDIR(root_inode.i_mode)) {
        printf("%d, %d, %d \n", root_inode.i_mtime, root_inode.i_ctime, root_inode.i_atime);
    }
}

void findInode(unsigned int inode_num, struct ext2_inode *inode) {
    unsigned int group_desc_index = inode_num / super.s_inodes_per_group;
    unsigned int inode_number_relative = inode_num % super.s_inodes_per_group;
    readInode(inode_number_relative, &group_descs[group_desc_index], &inode);
}

void inodeNumPermissions(unsigned int num) {
    struct ext2_inode entry_inode;
    unsigned int group_desc_index = num / super.s_inodes_per_group;
    unsigned int inode_number_relative = num % super.s_inodes_per_group;
    readInode(inode_number_relative, &group_descs[group_desc_index], &entry_inode);
    print_permissions(entry_inode.i_mode);
}

/**
 * Wyswietla zawartosc katalogu root
 */
void showInodeEntries(struct ext2_inode *inode) {
    if (S_ISDIR(inode->i_mode)) {
        printf("total: %d\n", inode->i_blocks);
        struct ext2_dir_entry *entry;
        unsigned int size;
        unsigned char block[block_size];

        lseek(fd, blockOffset(inode->i_block[0]), SEEK_SET);
        read(fd, block, block_size);                         /* read block from disk*/

        size = 0;                                            /* keep track of the bytes read */
        entry = (struct ext2_dir_entry *) block;           /* first entry in the directory */
        //int count = 0;
        while (size < inode->i_size) {
            //count++;
            char file_name[EXT2_NAME_LEN + 1];
            memcpy(file_name, entry->name, entry->name_len);
            file_name[entry->name_len] = 0;              /* append null char to the file name */
            inodeNumPermissions(entry->inode);
            printf("%d %s\n", entry->inode, file_name);

            //add_dir(entry->inode, file_name, entry->file_type);

            //add_dir(entry->inode, file_name, entry->file_type);
            if (entry->file_type == 2) {
                if (strcmp(file_name, ".") != 0 && strcmp(file_name, ".."))
                    add_dir(entry->inode, file_name, entry->file_type);
            }
            entry = (void *) entry + entry->rec_len;      /* move to the next entry */
            size += entry->rec_len;
            //printf("len: %d \n", entry->rec_len);
        }
        //printf("total: %d \n", count);
    }
}


int main(int argc, char* argvp[]) {
    openDisk();
    readSuperBlock();
    readGroups();
    readRoot();
    printf("\n\n%d %s:\n", 2, ".");
    showInodeEntries(&root_inode);
    //printf("==\n");
    //print_dirs();

    struct visitedDir *vd;
    vd = visited_dir_first;
    int flag = 0;
    while (flag == 0) {
        vd = visited_dir_first;
        while (vd != NULL) {
            if (vd->has_been_visited == 0) {
                flag = 1;
                vd->has_been_visited = 1;
                struct ext2_inode inode;
                unsigned int group_desc_index = vd->inode / super.s_inodes_per_group;
                unsigned int inode_number_relative = vd->inode % super.s_inodes_per_group;
                readInode(inode_number_relative, &group_descs[group_desc_index], &inode);
                printf("\n\n%d %s:\n", vd->inode, vd->file_name);
                showInodeEntries(&inode);
//                if (strcmp(vd->file_name, ".") != 0 && strcmp(vd->file_name, "..") != 0) {
//                    readInode(inode_number_relative, &group_descs[group_desc_index], &inode);
//                    print_permissions(inode.i_mode);
//                    printf("%d %s\n", vd->inode, vd->file_name);
//                    showInodeEntries(&inode);
//                }
            }
            vd = vd->next;

        }
        if (flag == 0)
            break;
    }

	return 0;
}
