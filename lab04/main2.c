#include <signal.h>
#include <stdio.h>
#include <unistd.h>

static void UsrHostSigAbort(int pSignal)
{
    // stopService();
    // Using printf is not good - see: http://stackoverflow.com/questions/16891019/
    // It will suffice for this code, however.
    printf("pankaj %d\n", pSignal);
}

static void HandleHostSignal(void)
{
    struct sigaction satmp;
    sigemptyset(&satmp.sa_mask);
    satmp.sa_flags = 0;
    satmp.sa_handler = UsrHostSigAbort;
    sigaction(SIGINT, &satmp, NULL);
}

int main(void)
{
    HandleHostSignal();

    while (1)
    {
        sleep(1);
        putchar('.');
        fflush(stdout);
    }
}
