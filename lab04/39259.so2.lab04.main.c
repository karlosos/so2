// SO2 IS1 212A LAB04
// Karol Dzialowski
// dk39259@zut.edu.pl

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

pid_t left_child_pid = 0;
pid_t right_child_pid = 0;
char* sciezka;
int while_flag = 1;

//
// konkatenacja cstringow z relokacja pamieci
//
char *konkatenancja(char *s1, const char *s2) {
	const size_t a = strlen(s1);
	const size_t b = strlen(s2);
	const size_t size_ab = a + b + 1;

	s1 = realloc(s1, size_ab);

	memcpy(s1 + a, s2, b + 1);

	return s1;
}

// blokuj sygnal stopu
void blokowanieSygnalow(void) {
	sigset_t iset;
	sigemptyset(&iset);
	sigaddset(&iset, SIGTSTP);
	sigprocmask(SIG_BLOCK, &iset, NULL);
}

// odblokuj sygnal stopu
void odblokowanieSygnalow(void) {
	sigset_t iset;
	sigemptyset(&iset);
	sigaddset(&iset, SIGTSTP);
	sigprocmask(SIG_UNBLOCK, &iset, NULL);
}

int nastepnaPotegaDwojki(int n);

// sprawdz czy blokowales sygnal stopu
int sprawdzBlokowaneSygnaly() {
	sigset_t blokowane;
	sigemptyset(&blokowane);
	sigpending(&blokowane);
	if(sigismember(&blokowane, SIGTSTP)) {
		return 1;
	}
	return 0;
}

// sygnal ctrl-c
static void sygnalInt(int signal)
{
	while_flag = 0;
}

// sygnal ctrl-z
static void sygnalStopu(int signal)
{
	//printf("Przyszedl sygnal stopu");
}

// ustawienie obslugi sygnalu ctrl-c
static void obslugaSygnalu(void)
{
	struct sigaction satmp;
	sigemptyset(&satmp.sa_mask);
	satmp.sa_flags = 0;
	satmp.sa_handler = sygnalInt;
	sigaction(SIGINT, &satmp, NULL);
}

// ustawienie obslugi sygnalu ctrl-z
static void obslugaSygnaluStopu(void) 
{
	struct sigaction satmp;
	sigemptyset(&satmp.sa_mask);
	satmp.sa_flags = 0;
	satmp.sa_handler = sygnalStopu;
	sigaction(SIGTSTP, &satmp, NULL);
}

int main(int argc, char **argv) {
	// program wywolywany z minimum jednym parametrem
	if (argc < 2)
		return -1;

	// utworzenie nowej grupy
	if (getpid() != getpgid(0))
		setpgid(0,0);

	blokowanieSygnalow();
	obslugaSygnalu();
	obslugaSygnaluStopu();	
	char *slowo;	
	int dlugosc_slowa = nastepnaPotegaDwojki(strlen(argv[1]));	
	slowo = malloc(sizeof(char) * dlugosc_slowa + 1);
	strcpy(slowo, argv[1]);
	// dopelnianie slowa
	if (dlugosc_slowa != strlen(argv[1])) {
		for (int i=strlen(argv[1]); i<dlugosc_slowa; i++)
			slowo[i] = 'a';
		slowo[dlugosc_slowa] = '\0';
	}
	sciezka = malloc(sizeof(char));
	sciezka[0] = '\0';
	if (argc > 2) {
		int dlugosc_sciezki = dlugosc_slowa + strlen(argv[2]);
		sciezka = konkatenancja(sciezka, argv[2]);	
		sciezka = konkatenancja(sciezka, " ");	
	}
	sciezka = konkatenancja(sciezka, slowo);
	//printf("Slowo: %s, sciezka %s \n", slowo, sciezka);

	if (dlugosc_slowa > 1) {
		left_child_pid = fork();
		if (left_child_pid == 0) {
			char *lewe_slowo;
			lewe_slowo = malloc(sizeof(char) * dlugosc_slowa/2 + 1);
			for (int i=0; i<dlugosc_slowa/2; i++)
				lewe_slowo[i] = slowo[i];
			lewe_slowo[dlugosc_slowa/2] = '\0';
			//printf("lewe slowo: %s", lewe_slowo);
			execl("./a.out", "a.out", lewe_slowo, sciezka, NULL);
		}	
		right_child_pid = fork();
		if (right_child_pid == 0) {
			char *prawe_slowo;
			prawe_slowo = malloc(sizeof(char) * dlugosc_slowa/2 + 1);
			int j = 0;
			for (int i=dlugosc_slowa/2; i<dlugosc_slowa; i++) {
				prawe_slowo[j] = slowo[i];
				j++;
			}
			prawe_slowo[dlugosc_slowa/2] = '\0';
			//printf("prawe slowo: %s \n", prawe_slowo);
			execl("./a.out", "a.out", prawe_slowo, sciezka, NULL);
		}	
		
		free(slowo);

		while(while_flag) {
		}

		kill(left_child_pid, SIGINT);
		kill(right_child_pid, SIGINT);
		
		if (waitpid(left_child_pid, NULL, 0) == -1) {
			printf("Lewe dziecko cos popsulo \n");
		}
		if (waitpid(right_child_pid, NULL, 0) == -1) {
			printf("Prawe dziecko cos popsulo \n");
		}
	}
	
	while(while_flag) {
	}

	printf("%d \t %s \n", (int) getpid(), sciezka);
	free(sciezka);
	
	int blokowany_sygnal = sprawdzBlokowaneSygnaly();
	if (blokowany_sygnal) {
		printf("Program zablokowal sygnal zatrzymania \n");
	}
	//obslugaSygnaluStopu();
	odblokowanieSygnalow();

	return 0;
}

// https://stackoverflow.com/a/108360
int nastepnaPotegaDwojki(int n) {
	unsigned count = 0;
	// zwroc liczbe jezeli jest potega dwojki
	if (!(n&(n-1)))
				return n;
	// wylicz najblizsza potege dwojki	
	while( n != 0) {
		n  >>= 1;
		count += 1;
	}
	return 1 << count;
}

