#include <stdio.h>
#include <utmp.h>
#include <unistd.h>
#include <stdlib.h>
#include <grp.h>
#include <pwd.h>

/*
 * Program wyswietla aktualnie zalogowanych uzytkownikow. 
 * Przelacznik -g wyswietla grupy do ktorych nalezy uzytkownik
 * Przelacznik -h wyswietla host z jakiego zalogowany jest uzytkownik
 *
 * Pobieranie informacji o logowaniach uzywa funkcji getutent
 * Pobieranie informacji o grupach uzywa funkcji przykladowego kodu z manuala getgrouplist
 * Pobieranie parametrow korzysta z getopt - przykladowy kod z wykladu 1
 */

int main(int argc, char **argv) {
  // hflag - przelacznik wyswietlania hostow
  // gflag - przelacznik wyswietlania grup
	int hflag = 0, gflag = 0, ret, index;
	opterr = 0;

	while ((ret = getopt(argc, argv, "hg")) != -1)
		switch (ret) {
			case 'h': hflag = 1; break;
			case 'g': gflag = 1; break;
			case '?':
								fprintf(stderr, "Uknown option '-%c'.\n", optopt);
								return 1;
			default: abort();
		}
	

	struct utmp* record;
	while(1) {
		record = getutent();
		if (record == NULL) 
			break;

    int ut_type = record->ut_type;
		if (ut_type == USER_PROCESS) {
			printf("%s", record->ut_user);
			
      // wyswietlanie hosta
			if (hflag == 1) {
				printf(" (%s)", record->ut_host);
			}
		  
      // wyswietlanie grup  
			if (gflag == 1) {
        // maksymalna ilosc grup do pobrania (jezeli zwroci wiecej to nie wyswietli)
				int ngroups = 0;
				gid_t *groups;
				struct passwd *pw;
				struct group *gr;
				// fetch passwd structure (contains first group id for user)
				pw = getpwnam(record->ut_user);
				if (pw == NULL) {
					perror("getpwnam");
					free(groups);
					break;
				}
				
				getgrouplist(record->ut_user, pw->pw_gid, groups, &ngroups);
				groups = malloc(ngroups* sizeof(gid_t));
					
				if (getgrouplist(record->ut_user, pw->pw_gid, groups, &ngroups) == -1) {
					fprintf(stderr, "getgrouplist() zwrocil za duzo grup");
					free(groups);
					break;
				}

				// display list of retrieved groups
				printf(" [");
				for (int i=0; i<ngroups; i++) {
					gr = getgrgid(groups[i]);
					if (gr != NULL)
						if (i==ngroups-1)
							printf("%s", gr->gr_name);
						else 
							printf("%s, ", gr->gr_name);
				}
				printf("]");
        free(groups);
			}
			printf("\n");	
		}
	}

	return 0;
}
