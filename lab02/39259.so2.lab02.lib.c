// SO2 IS1 212A LAB02
// Karol Dzialowski
// dk39259@zut.edu.pl

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <grp.h>
#include <pwd.h>
#include <string.h>
#include "libgroups.h"

//
// konkatenacja cstringow z relokacja pamieci
//
char * my_strcatt(char *s1, const char *s2) {
    const size_t a = strlen(s1);
    const size_t b = strlen(s2);
    const size_t size_ab = a + b + 1;

    s1 = realloc(s1, size_ab);

    memcpy(s1 + a, s2, b + 1);

    return s1;
}

//
// Funkcja pobiera nazwe uzytkownika i zwraca liste grup do ktorych nalezy
// w postaci lancucha "[grupa1, grupa2, grupa3, ..., grupan]"
//
char* getGroups(const char *ut_user) {
	char *str;
	str = malloc(sizeof(char));	
  str[0] = '\0';
	int ngroups = 0;
	gid_t *groups;
	struct passwd *pw;
	struct group *gr;
	pw = getpwnam(ut_user);
	if (pw == NULL) {
		perror("getpwnam");
		free(groups);
		return "";
	}
	
	getgrouplist(ut_user, pw->pw_gid, groups, &ngroups);
	groups = malloc(ngroups* sizeof(gid_t));
		
	if (getgrouplist(ut_user, pw->pw_gid, groups, &ngroups) == -1) {
		fprintf(stderr, "getgrouplist() zwrocil za duzo grup");
		free(groups);
		return "";
	}

  str = my_strcatt(str, "[");
   
	for (int i=0; i<ngroups; i++) {
		gr = getgrgid(groups[i]);
		if (gr != NULL) {
      str = my_strcatt(str, gr->gr_name);
			if (i!=ngroups-1) {
        str = my_strcatt(str, ", "); 
      }
    }
  }

  str = my_strcatt(str, "]"); 
	free(groups);
	return(str);
}
