// SO2 IS1 212A LAB02
// Karol Dzialowski
// dk39259@zut.edu.pl

#include <stdio.h>
#include <utmp.h>
#include <unistd.h>
#include <stdlib.h>
#include <grp.h>
#include <pwd.h>
#include <dlfcn.h>
/*
 * Program wyswietla aktualnie zalogowanych uzytkownikow. 
 * Przelacznik -g wyswietla grupy do ktorych nalezy uzytkownik
 * Przelacznik -h wyswietla host z jakiego zalogowany jest uzytkownik
 *
 * Pobieranie informacji o logowaniach uzywa funkcji getutent
 * Pobieranie informacji o grupach uzywa funkcji przykladowego kodu z manuala getgrouplist
 * Pobieranie parametrow korzysta z getopt - przykladowy kod z wykladu 1
 */
char* (*getGroups) (const char *);
int main(int argc, char **argv) {
  // hflag - przelacznik wyswietlania hostow
  // gflag - przelacznik wyswietlania grup
	int hflag = 0, gflag = 0, ret, index;
	opterr = 0;

	while ((ret = getopt(argc, argv, "hg")) != -1)
		switch (ret) {
			case 'h': hflag = 1; break;
			case 'g': gflag = 1; break;
			case '?':
								fprintf(stderr, "Uknown option '-%c'.\n", optopt);
								return 1;
			default: abort();
		}
	

	struct utmp* record;
	while(1) {
		record = getutent();
		if (record == NULL) 
			break;

    int ut_type = record->ut_type;
		if (ut_type == USER_PROCESS) {
			printf("%s", record->ut_user);
			
      // wyswietlanie hosta
			if (hflag == 1) {
				printf(" (%s)", record->ut_host);
			}
		  
      // wyswietlanie grup  
			if (gflag == 1) {
        // ladowanie biblioteki
        void *handle = dlopen( "./libgroups.so", RTLD_LAZY );
        if (!handle)
          dlerror();
        else {
          getGroups = dlsym(handle, "getGroups");
					//if (getGroups != NULL) {
					if (dlerror() == NULL) {
						char* groups = getGroups(record->ut_user);
						printf(" %s", groups);
						free(groups);
						dlclose(handle);
					}
        }
			}

			printf("\n");	
		}
	}

	return 0;
}
