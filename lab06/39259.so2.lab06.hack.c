// SO2 IS1 212A LAB06
// Karol Dzialowski
// dk39259@zut.edu.pl

#define _GNU_SOURCE
#include <fcntl.h>
#include <sys/stat.h>
#include <assert.h>
#include <memory.h>
#include <pthread.h>
#include <crypt.h>
#include <time.h>
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "time.h"
#include "sys/mman.h"

int has_cracked_password = 0;
pthread_mutex_t has_cracked_password_mutex=PTHREAD_MUTEX_INITIALIZER;
int completed_threads = 0;
pthread_mutex_t completed_threads_mutex=PTHREAD_MUTEX_INITIALIZER;
char* salt = NULL;
char* password = NULL;
int* bytes_processed = NULL;
char* cracked_password = NULL;

struct reading_file_args {
    void* mmapedData;
    size_t start;
    size_t end;
    size_t filesize;
    int thread_number;
};

/**
 * Change value of num_of_threads if it's greater than number of avaiable processors
 *
 * @param[in,out] num_of_threads
 */
void checkNumberOfProcessors(int* num_of_threads);

/**
 * Get file size of given file name
 *
 * @param[in] filename
 * @return size of file
 */
size_t getFileSize(const char *filename);

/**
 *
 * @param arguments : struct reading_file_args
 * @return
 */
void* readFile(void *arguments);

/**
 * Calculate difference between two timespecs
 *
 * https://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/
 *
 * @param start
 * @param end
 * @return struct timespec
 */
struct timespec diff(struct timespec start, struct timespec end);

void createThreads(char* filename, int num_of_threads);

char* getSaltFromPassword(char* password);

int main(int argc, char * argv[]) {
    char* file;
    int num_of_threads;
    // Not enough arguments
    if (argc < 3) {
        printf("Wywolaj z trzema argumentami: ./a.out haslo plik_z_haslami ilosc_watkow \n");
        return -1;
    }
    // Test mode
    if (argc == 3) {
        printf("Tryb testowania\n");
        password = malloc(sizeof(char) * (strlen(argv[1]) + 1));
        strcpy(password, argv[1]);
        file = argv[2];
        num_of_threads = 0;
    }
    // Regular cracking mode
    if (argc == 4) {
        password = malloc(sizeof(char) * (strlen(argv[1]) + 1));
        strcpy(password, argv[1]);
        file = argv[2];
        num_of_threads = atoi(argv[3]);
    }

    // Avaiable processors
    checkNumberOfProcessors(&num_of_threads);
    // TODO nie wyswietla dolara na uczelnianym
    printf("%s, %s, %d \n", password, file, num_of_threads);
    salt = getSaltFromPassword(password);

    // Regular cracking mode
    if(num_of_threads > 0) {
        createThreads(file, num_of_threads);
    }
    // Threads testing
    else {
        password[0] = '\0';
        // Get max number of threads
        int num_of_processors = sysconf(_SC_NPROCESSORS_ONLN);
        // Test for every avaiable number of threads
        for (int num_of_threads=1; num_of_threads<= num_of_processors; num_of_threads++) {
            struct timespec time1, time2;
            clockid_t clk_id;
            /*
             * CLOCK_MONOTONIC
             * Clock that cannot be set and represents monotonic time since some unspecified starting point.
             * This clock is not affected by discontinuous jumps in the system time (e.g., if
             * the system administrator manually changes the clock), but is affected by the incremental
             * adjustments performed by adjtime(3) and NTP.
             */
            clk_id = CLOCK_MONOTONIC;
            clock_gettime(clk_id, &time1);
            createThreads(file, num_of_threads);
            // Clock stop
            clock_gettime(clk_id, &time2);
            printf("Threads: %d, time: %d:%d\n", num_of_threads, (int)diff(time1, time2).tv_sec, (int)diff(time1, time2).tv_nsec);
        }
    }
    free(salt);
    free(password);
    return 0;
}

void checkNumberOfProcessors(int* num_of_threads) {
    int num_of_processors = sysconf(_SC_NPROCESSORS_ONLN);
    if (*num_of_threads > num_of_processors)
        *num_of_threads = num_of_processors;
}

size_t getFileSize(const char *filename) {
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}

void createThreads(char* file, int num_of_threads) {
    // Pointer to mmap
    void* mmappedData;
    size_t filesize = getFileSize(file);
    if (password[0] == '\0') {
        if (filesize > 10000)
            filesize = 10000;
    }
    int fd = open(file, O_RDONLY, 0);
    assert(fd != -1);

    // Map file
    mmappedData = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE | MAP_POPULATE, fd, 0);
    assert(mmappedData != MAP_FAILED);

    pthread_t threads[num_of_threads];
    bytes_processed = malloc(sizeof(int) * num_of_threads);
    for (int i=0; i<num_of_threads; i++) {
        bytes_processed[i] = 0;
    }
    struct reading_file_args arguments[num_of_threads];
    size_t size_per_thread = filesize / num_of_threads;
    // Configure thread arguments (data offsets)
    // read file with mmap multiple threading in C
    for (int i = 0; i < num_of_threads; i++) {
        arguments[i].mmapedData = mmappedData;
        arguments[i].start = i * size_per_thread;
        //arguments[i].end = (i + 1) * size_per_thread;
        arguments[i].end = (size_t)(memchr(mmappedData + (i + 1) * size_per_thread, (int)'\n', 250) - mmappedData);
        if (arguments[i].end > filesize || arguments[i].end < 0) {
            arguments[i].end = filesize;
        }
        arguments[i].filesize = filesize;
        //printf("end1: %d, end2: %d, fs: %d \n", (i + 1) * size_per_thread, arguments[i].end, filesize);
        arguments[i].thread_number = i;
    }

    // Create threads
    for (int i = 0; i < num_of_threads; i++) {
        pthread_create(&threads[i], NULL, readFile, (void *) &arguments[i]);
    }

   	int j = 0;
	 	while (completed_threads < num_of_threads) {
				if (j % 900 == 0) {
        int sum_processed_bytes = 0;
        for (int i=0; i<num_of_threads; i++) {
            sum_processed_bytes += bytes_processed[i];
        }
        if (password[0] != '\0')
            printf("%f \n", ((1.f - (float)sum_processed_bytes/filesize)));
    		j = 0;
			} else {
				j++;
			}
		}

    // Wait for threads
    for (int i = 0; i < num_of_threads; i++) {
        pthread_join(threads[i], NULL);
    }

    // Unmap file
    int rc = munmap(mmappedData, filesize);
    assert(rc == 0);
    // Close file
    close(fd);

    if (has_cracked_password) {
        // TODO print password
        printf("%s\n", cracked_password);
        free(cracked_password);
    } else {
        printf("Nie znaleziono hasla \n");
    }

    free(bytes_processed);
}

void* readFile(void *arguments) {

    // Get function arguments
    struct reading_file_args* args = (struct reading_file_args*)arguments;
    void* mmappedData = args->mmapedData;
    //printf("readFile args: %d, %d, %d \n", args->thread_number, args->start, args->end);
    int start = args->start;
    int end = args->end;
    size_t filesize = args->filesize;
    int thread_number = args->thread_number;
    // Flag to end reading data
    int flag = 1;
    // Set offset
    void *reading = mmappedData + start;
    while (flag) {
        // Maximum length of line
        size_t how_much_to_read = 250;
        if (reading + how_much_to_read > mmappedData + end) {
            //printf("Calculated end: %ld, real end: %ld \n", reading + how_much_to_read, mmappedData + end);
            how_much_to_read = (int) ((mmappedData + filesize) - reading - 1);
            //how_much_to_read = (int) ((mmappedData + end) - reading - 1);
            //printf("New end: %d \n", (int) ((mmappedData + end) - reading - 1));
        }
        // Find end of line position
        void *end_of_line = memchr(reading, (int) '\n', how_much_to_read);
        // Calculate line length
        int line_len = 0;
        if (end_of_line != NULL)
            line_len = (int) (end_of_line - reading);
        if (how_much_to_read <= 0)
            line_len = 0;

        //printf("%d : Line len: %d \n", thread_number, line_len);
        if (line_len > 0) {
            // Make line cstring
            char *line = malloc(sizeof(char) * line_len + 1);
            memcpy(line, reading, line_len);
            line[line_len] = '\0';

            // Encrypt line
            struct crypt_data cd;
            cd.initialized = 0;
            char *enc = crypt_r(line, salt, &cd);
            //printf("%s\n", line);
            // If password is cracked
            //printf("%d : %s\n", thread_number, line);
            //printf("%s \n", line, enc, password, salt);
            if (strcmp(enc, password) == 0) {
                //printf("Haslo: %s\n", line);
                // Lock has_cracked_password
                pthread_mutex_lock( &has_cracked_password_mutex);
                has_cracked_password = 1;
                cracked_password = malloc(sizeof(char) * strlen(line));
                strcpy(cracked_password, line);
                pthread_mutex_unlock( &has_cracked_password_mutex);
                flag = 0;
            }

            free(line);
            if (reading > mmappedData + end)
                flag = 0;
            reading += line_len + 1;


            //program_percentage++;
            //float percentage = (1.f - (float)(mmappedData + end - reading)/(end - start));
            //printf("%f\n", percentage);
            bytes_processed[thread_number] = mmappedData + end - reading;

            if (has_cracked_password)
                flag = 0;
        } else {
            flag = 0;
        }
    }
    // Lock completed threads
    pthread_mutex_lock( &completed_threads_mutex);
    completed_threads++;
    pthread_mutex_unlock( &completed_threads_mutex);
}

struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

char* getSaltFromPassword(char* password)
{
    // find third occurence of $
    // $6$salt$password
    char* pch = password;
    for (int i=0; i<3; i++) {
        pch = strchr(pch, '$');
        if (pch == NULL)
            return NULL;
        pch = pch+1;
    }

    int salt_len = pch-password+1;
    char* salt = malloc(sizeof(char) * (salt_len));
    strncpy(salt, password, salt_len);
    salt[salt_len-1] = '\0';
    return salt;
}
