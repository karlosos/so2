// SO2 IS1 212A LAB06
// Karol Dzialowski
// dk39259@zut.edu.pl

#define _GNU_SOURCE
#include <crypt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *my_encrypt(char *pass, char *s);

int main(int argc, char * argv[]) {
	printf("%s\n", my_encrypt(argv[1], argv[2]));
	return 0;
}

char *my_encrypt(char *pass, char *s) {
	char *salt = malloc(sizeof(char)*(strlen(s)+5));
	sprintf(salt,"$6$%s$", s);
	struct crypt_data cd;
	cd.initialized = 0;
	char *enc = crypt_r(pass, salt, &cd);
	return enc;
}
