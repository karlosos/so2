// SO2 IS1 212A LAB05
// Karol Dzialowski
// dk39259@zut.edu.pl

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <limits.h>

int number_of_files;
int number_of_dirs;

int d_flag = 0;
int f_flag = 0;
int l_flag = 0;

char *concat(char *s1, const char *s2);

void printIndentation(int count);

void scanDir(char *dir_path, int indent, int level);

int wasVisited(ino_t inode);

struct visited_symlink {
	ino_t node;
	struct visited_symlink* next;
};

struct visited_symlink* first_vs = NULL;

int main(int argc, char **argv) {

	number_of_files = 0;
	number_of_dirs = 0;

	int ret, index;
	int L_value = INT_MAX;

	opterr = 0;

	while ((ret = getopt(argc, argv, "dflL:")) != -1)
		switch (ret) {
			case 'd':
				d_flag = 1;
				break;
			case 'f':
				f_flag = 1;
				break;
			case 'l':
				l_flag = 1;
				break;
			case 'L':
				L_value = atoi(optarg);
				break;
			case '?':
				if (optopt == 'L')
					fprintf(stderr, "Opcja -%c wymaga argumentu.\n", optopt);
				else
					fprintf(stderr, "Uknown option '-%c'.\n", optopt);
				return 1;
			default:
				abort();
		}

	/*
	// sprawdz flagi
	if (d_flag)
	printf("d_flag \n");
	if (f_flag)
	printf("f_flag \n");
	if (l_flag)
	printf("l_flag \n");
	printf("L flaga: %d \n", L_value);

	for (index = optind; index < argc; index++)
	printf("Non-option argument %s\n", argv[index]);
	*/

	char *dir_path;
	// if path not set then make it local folder "."
	if (optind == argc) {
		dir_path = malloc(sizeof(char) * 2);
		strcpy(dir_path, ".\0");
	}
	else {
		dir_path = malloc(sizeof(char) * (strlen(argv[optind]) + 1));
		strcpy(dir_path, argv[optind]);
	}

	printf("%s \n", dir_path);

	scanDir(dir_path, 1, L_value-1);
	free(dir_path);
	printf("dirs: %d\n", number_of_dirs);
	printf("files: %d\n", number_of_files);
	return 0;
}

char *concat(char *s1, const char *s2) {
	const size_t a = strlen(s1);
	const size_t b = strlen(s2);
	const size_t size_ab = a + b + 1;

	s1 = realloc(s1, size_ab);

	memcpy(s1 + a, s2, b + 1);

	return s1;
}

void printIndentation(int count) {
	for (int i = 0; i < count; i++)
		printf("--");
}

void scanDir(char *dir_path, int indent, int level) {
	DIR *dir = opendir(dir_path);
	if (dir == NULL)
		return;

	struct dirent *dir_entry;
	while (dir_entry = readdir(dir)) {
		// full path to read entry
		char *entry_path = malloc(sizeof(char) * (strlen(dir_path) + 1));
		strcpy(entry_path, dir_path);
		entry_path = concat(entry_path, "/");
		entry_path = concat(entry_path, dir_entry->d_name);

		// directory
		if (dir_entry->d_type == DT_DIR) {
			if (strcmp(dir_entry->d_name, "..") != 0 && strcmp(dir_entry->d_name, ".")) {
				if (dir_entry->d_name[0] == '.') {
					free(entry_path);  
					continue;
				}
				// TODO add directory to visited list not only symbolic links to directories
				// in case when symbolic link points to parent of directory which was visited 
				// but parent itself wasn't scanned
				/*
					 struct stat sl;
					 stat(entry_path, &sl);
					 if (!wasVisited(sl.st_ino)) {
					 	
				struct visited_symlink *vs = malloc(sizeof(struct visited_symlink));
				vs->next = first_vs;
				vs->node = sl.st_ino;
				//printf("%s, %d \n", entry_path, (int)vs->node);
				first_vs = vs;
				*/
				number_of_dirs++;
				printIndentation(indent);
				if (f_flag)
					printf("%s/", dir_path);
				printf("%s \n", dir_entry->d_name);
				if (level > 0) {
					scanDir(entry_path, indent + 1, level - 1);
				}
			}
			} 
			// regular file
			else if (dir_entry->d_type == DT_REG && d_flag == 0) {
				if (dir_entry->d_name[0] == '.') {
					free(entry_path);  
					continue;
				}
				number_of_files++;
				printIndentation(indent);
				if (f_flag)
					printf("%s/", dir_path);
				printf("%s \n", dir_entry->d_name);
			}
			// symbolic link
			if (dir_entry->d_type == DT_LNK) {
				struct stat sb;
				char *linkname;
				ssize_t r, bufsiz;

				if (lstat(entry_path, &sb) == -1) {
					perror("lstat");
					exit(EXIT_FAILURE);
				}

				bufsiz = sb.st_size + 1;

				// code from readlink manual
				/* Some magic symlinks under (for example) /proc and /sys
					 report 'st_size' as zero. In that case, take PATH_MAX as
					 a "good enough" estimate */
				if (sb.st_size == 0)
					bufsiz = PATH_MAX;

				linkname = malloc(bufsiz);
				if (linkname == NULL) {
					perror("malloc");
					exit(EXIT_FAILURE);
				}

				r = readlink(entry_path, linkname, bufsiz);
				if (r == -1) {
					perror("readlink");
					exit(EXIT_FAILURE);
				}

				linkname[r] = '\0';

				//printf("'%s' points to '%s'\n", entry_path, linkname);

				if (r == bufsiz)
					printf("(Returned buffer may have been truncated)\n");

				struct stat sl;
				stat(linkname, &sl);
				// symbolic link directory
				if (S_ISDIR(sl.st_mode)) {
					number_of_dirs++;
					printIndentation(indent);
					if (f_flag)
						printf("%s/", dir_path);

					if (level > 0 && l_flag) {
						if (!wasVisited(sl.st_ino)) {
							// dodac do listy sl.st_ino typ ino_t
							printf("%s -> %s \n", dir_entry->d_name, linkname);
							struct visited_symlink *vs = malloc(sizeof(struct visited_symlink));
							vs->next = first_vs;
							vs->node = sl.st_ino;
							first_vs = vs;

							scanDir(linkname, indent + 1, level - 1);
						} else {
							printf("%s -> %s [recursive, not followed] \n", dir_entry->d_name, linkname);
						}
					} else {
						printf("%s -> %s \n", dir_entry->d_name, linkname);
					}
				} else if (S_ISREG(sl.st_mode)) {
					// symbolic link regular file
					number_of_files++;
					printIndentation(indent);
					if (f_flag)
						printf("%s/", dir_path);
					printf("%s -> %s \n", dir_entry->d_name, linkname);
				}
				free(linkname);
			}
			free(entry_path);
		}
		free(dir);
	}

	int wasVisited(ino_t inode) {
		struct visited_symlink* vs = first_vs;
		while (vs != NULL) {
			if (vs->node == inode)
				return 1;
			vs = vs->next;
		}
		return 0;
	}
